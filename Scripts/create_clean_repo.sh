#!/bin/bash
set -e
mkdir clean_repo
cd clean_repo
git init
touch f
git add f
if [[ $(uname) == "Darwin" ]] ; then
    DATE="$(gdate -d @100000)"
else
    DATE="$(date -d @100000)"
fi
git commit -m "First commit at $DATE (unix 100000)"
echo git commit --no-edit --amend --date "$DATE"
GIT_COMMITTER_DATE="$DATE" git commit --no-edit --amend --date "$DATE"
