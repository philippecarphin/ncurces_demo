#include <ncurses.h>
#include "ncurses_demo.h"
#include <string.h>
#include <libgen.h>

WINDOW *my_ncurses_init(){

    WINDOW* main_window = initscr();			/* Start curses mode 		  */
    if(main_window == NULL){
        return NULL;
    }
    int err = 0;

    err = noecho();
    if(err){
        return NULL;
    }

    err = raw();
    if(err){
        return NULL;
    }

    err = keypad(stdscr, TRUE);
    if(err){
        printf("ERROR : Failed to enable keypad F1, F2, Arrows, ...\n");
        return NULL;
    }

    return main_window;
}

int get_user_name(WINDOW* w, int y, int x)
{
    int err;
    char name[100];

    mvwprintw(w, y, x,"Enter your name > ");

    err = echo();
    if(err){
        return err;
    }

    // scanw("%s", name); // Only gets the first word
    // To get a string terminated by a newline, use
    attron(A_BOLD);
    wgetstr(w, name);
    attroff(A_BOLD);

    err = noecho();
    if(err){
        return err;
    }

    mvwprintw(w, y+2, x, "You entered '");
    attron(A_BOLD);
    wprintw(w, "%s", name);
    attroff(A_BOLD);
    wprintw(w, "'");

    return 0;
}

int print_dimensions(WINDOW* w, int y, int x)
{
    int err = 0;
    int n_rows = 0;
    int n_cols = 0;
    // NOTE: This is a macro even if the documentation shows
    // it as 'void getmaxyx(WINDOW*, int, int)'.  There is no
    // such declaration.
    getmaxyx(w, n_rows, n_cols);
    if(n_rows == 0 || n_cols == 0){
        return 1;
    }
    mvwprintw(w, y, x,"Dimensions are n_rows=%d, n_cols=%d", n_rows, n_cols);

    return 0;
}

int ncurses_demo(int argc, char **argv){
    int err = 0;
    WINDOW *main_window = my_ncurses_init();
    if(main_window == NULL){
        goto end;
    }

    err = print_dimensions(main_window, 7,0);
    if(err){
        goto end;
    }

    err = get_user_name(main_window, 9, 0);
    if(err){
        goto end;
    }

    getch();
    getch();

end:
    endwin();
    return err;
}

int get_modified_state(const char *path, int *modified, int *untracked)
{
    char output[10000];
    output[0] = '\0';

    char cmd[1000];
    snprintf(cmd, 1000, "cd %s && git status 2>&1", path);
    FILE *fp = popen(cmd, "r");
    if(fp == NULL){
        return 1;
    }

    *modified = 0;
    *untracked = 0;
    char buff[100];
    while(fgets(buff, sizeof(buff), fp) != NULL){
        if(strstr(buff, "Changes to be committed:") != NULL){
            // add the filename to a list or something
            *modified = 1;
        }
        if(strstr(buff, "Changes not staged for commit:") != NULL){
            // add the filename to a list or something
            *modified = 1;
        }
        if(strstr(buff, "Untracked files:") != NULL){
            // add the filename to a list or something
            *untracked = 1;
        }
    }

out_close:
    pclose(fp);
    return 0;
}

int get_time_since_last_commit(const char *path, long unsigned int *last_commit_timestamp)
{
    char cmd[1000];
    snprintf(cmd, 1000, "cd %s && git log --pretty=format:%%at 2>&1", path);
    FILE *fp = popen(cmd, "r");
    if(fp == NULL){
        return 1;
    }
    int unix_timestamp;

    char buff[100];
    // I think I can just do sscanf here
    while(fgets(buff, sizeof(buff), fp) != NULL){
        sscanf(buff, "%u", &unix_timestamp);
    }

    *last_commit_timestamp = unix_timestamp;

    return 0;
}

int get_complete_info(const char *path, struct RepoInfo* repo_info)
{
    snprintf(repo_info->config.path, 1000, "%s", path);
    snprintf(repo_info->config.name, 1000, "%s", basename((char*)path));
    snprintf(repo_info->config.short_name, 1000, "%s", basename((char*)path));
    repo_info->config.fetch = 1;
    snprintf(repo_info->config.comment, 1000, "check");


    int err = get_modified_state(path, &(repo_info->state.dirty), &(repo_info->state.untracked_files));
    if(err){
        return err;
    }

    err = get_time_since_last_commit(path, &(repo_info->state.time_since_last_commit));
    if(err){
        return err;
    }

    return 0;
}

int get_data_from_yaml()
{
    return 0;
}
