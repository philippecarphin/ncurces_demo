
WINDOW *my_ncurses_init();
int get_modified_state(const char *path, int *modified, int *untracked);
int get_time_since_last_commit(const char *path, long unsigned int *last_commit_timestamp);

