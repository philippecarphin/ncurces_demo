#ifndef _NCURSES_DEMO_H
#define _NCURSES_DEMO_H

#include <ncurses.h>

int ncurses_demo(int argc, char **argv);
// Structs for describing the state of a repo
struct RepoState {
    int dirty;
    int untracked_files;
    long unsigned int time_since_last_commit;
};
struct RepoConfig {
    char path[1000];
    char name[1000];
    char short_name[1000];
    int fetch;
    char comment[1000];
};
struct RepoInfo {
    struct RepoConfig config;
    struct RepoState state;
};

int get_complete_info(const char *path, struct RepoInfo* repo_info);


// Program configuration
struct Config {
    int color;
    struct RepoConfig defaults;
};

// File containting the list of repos and configurations
struct RepoFile {
    struct RepoConfig repos[1000];
    struct Config config;
};

// Program arguments
struct Args {
    char command[1000];
    char path[1000];
    char name[1000];
    int generate_config;
};

int get_data_from_yaml();
#endif
